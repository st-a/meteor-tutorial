// contains the name of the message, that is shown at the moment
var showMessage = false;
var showMessageDepenency = new Tracker.Dependency;
var messageText = "";

if (Meteor.isClient) {
  // create Collection containing Messages
  // data will not be stored in MongoDB, because of argument "null"
  // the keyword "var" is omitted, so "Messages" will be global accessible from other files
  Messages = new Meteor.Collection(null);
  // load collection data, see file: data.js
  loadData();

  // counter starts at 0
  Session.setDefault("counter", 0);

  Template.body.helpers({
    showMessage: function () {
      // run every time showMessage was changed
      showMessageDepenency.depend();
      return showMessage;
    }
  });

  Template.hello.helpers({
    counter: function () {
      return Session.get("counter");
    },
    counterGreaterZero: function () {
      // returns true if counter is greater than zero
      return Session.get("counter") > 0;
    },
    showMessage: function () {
      // run every time showMessage was changed
      showMessageDepenency.depend();
      return showMessage;
    }
  });

  Template.hello.events({
    'click button#add': function () {
      // increment the counter when button is clicked
      Session.set("counter", Session.get("counter") + 1);
      // is there a message for current count?
      var message = Messages.findOne({count: Session.get("counter")});
      // show message 
      if (typeof(message) != "undefined") {
        showMessage = true;
        // get message text from Collection
        messageText = message.text;
        // update dependencies
        showMessageDepenency.changed();
      }
    },
    'click button#sub': function () {
      // decrement the counter when button is clicked
      Session.set("counter", Session.get("counter") - 1);
    }
  });

  Template.message.helpers({
    messageText: function () {
      showMessageDepenency.depend();
      return messageText;
    }
  });

  Template.message.events({
    'click button.close': function () {
      // clear current message
      showMessage = false;
      // update dependencies
      showMessageDepenency.changed();
    }
  });

  Template.message.rendered = function() {
    // jQuery code specific to the message template would go here
    // do not use "$(document).ready()"
    alert("There is a new Message.");
  };
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
